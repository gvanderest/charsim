import React from 'react';
import Character from '../models/Character.js';

export default class Simulator extends React.Component {
    constructor() {
        super();
        this.state = {
            character: new Character()
        };
    }
    getCharacter() {
        return this.state.character;
    }
    changeName() {
        let character = this.getCharacter();
        let name = window.prompt("What should the new name be?");

        try {
            character.setName(name);
            this.forceUpdate();
        } catch (e) {
            if (e.type === "invalid_name") {
                window.alert("Sorry, that's an invalid name.");
            } else {
                throw e;
            }
        }
    }
    increaseStat(stat) {
        stat.increase(1);
        this.forceUpdate();
    }
    reduceStat(stat) {
        stat.decrease(1);
        this.forceUpdate();
    }
    renderStats() {
        let stats = [];

        this.getCharacter().getStats().forEach((stat) => {
            stats.push(
                <div className={ 'stat ' + stat.id } key={ stat.id }>
                    <label>{ stat.id.toUpperCase() }</label>
                    <var>
                        <span className="base">{ stat.getBase() }</span>
                        <span className="plus">+</span>
                        <span className="bonus">{ stat.getBonus() }</span>
                        <span className="cost">({ stat.getCostOfIncrease() || '-' })</span>
                        <span className="minus clickable" onClick={ this.reduceStat.bind(this, stat) }>&lt;</span>
                        <span className="plus clickable" onClick={ this.increaseStat.bind(this, stat) }>&gt;</span>
                    </var>
                </div>
            );
        });

        return stats;
    }
    render() {
        return (
            <div className="simulator">
                <h1>Ragnarok Online Simulator</h1>
                <div className="window status">
                    <div className="name" onClick={ this.changeName.bind(this) }>{ this.getCharacter().getName() }</div>
                    { this.renderStats() }
                </div>
            </div>
        );
    }
}
