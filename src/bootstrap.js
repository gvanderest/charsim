import React from 'react';
import ReactDOM from 'react-dom';
import Simulator from './components/Simulator.js';

ReactDOM.render(<Simulator />, document.getElementById('simulator'));
