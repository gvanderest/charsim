let STAT_IDS = ['str', 'agi', 'vit', 'int', 'dex', 'luk'];
let STAT_BASE_MIN = 1;
let STAT_BASE_MAX = 99;
class Stat {
    constructor(data) {
        this.base = 1;
        this.bonus = 0;

        Object.assign(this, data);
    }

    getBase() {
        return this.base;
    }

    getBonus() {
        return this.bonus;
    }

    getTotal() {
        return this.base + this.bonus;
    }

    getCostOfIncrease() {
        return 2;
    }

    increase(amount) {
        if (amount === undefined) {
            amount = 1;
        }

        // FIXME add min/max constants
        this.base += amount;
        this.base = Math.min(STAT_BASE_MAX, this.base);
        this.base = Math.max(STAT_BASE_MIN, this.base);
    }

    decrease(amount) {
        if (amount === undefined) {
            amount = 1;
        }
        this.increase(-1 * amount);
    }
}

class Substat extends Stat {
}

export default class Character {
    constructor(data) {
        this.name = 'gn0me';
        this.jobId = 'novice';
        this.stats = {};
        STAT_IDS.forEach((statId) => {
            this.stats[statId] = new Stat({
                id: statId,
                base: 1,
                bonus: 0,
                character: this
            });
        });

        Object.assign(this, data);
    }

    getStats() {
        let stats = [];
        Object.keys(this.stats).forEach((key) => {
            stats.push(this.stats[key]);
        });
        return stats;
    }

    getSubstats() {
        return this.substats;
    }

    getStat(id) {
        return this.stats[id];
    }

    getSubstat(id) {
        return this.substats[id];
    }

    getName() {
        return this.name;
    }

    setName(name) {
        let cleaned = name.trim();
        if (!cleaned) {
            throw { type: "invalid_name" };
        }
        this.name = cleaned;
    }
}
