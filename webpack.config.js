/*global module, require, __dirname*/

var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'release');
var APP_DIR = path.resolve(__dirname, 'src');

var config = {
    entry: APP_DIR + '/bootstrap.js',
    devtool: 'source-map',
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js',
        sourceMapFilename: 'bundle.js.map'
    },
    module : {
        loaders : [
            {
                test : /\.jsx?/,
                include : APP_DIR,
                loader : 'babel'
            }
        ]
    },
    plugins: [
        // new webpack.DefinePlugin({
        //     'process.env': {
        //         'NODE_ENV': JSON.stringify('production')
        //     }
        // }),
        // new webpack.optimize.UglifyJsPlugin({
        //     sourceMap: true,
        //     mangle: true,
        //     output: {
        //         comments: false
        //     }
        // })
    ],
    devServer: {
        historyApiFallback: true
    }
};

module.exports = config;
